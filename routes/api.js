var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var MongoClient = require('mongodb').MongoClient


/* GET users listing. */
router.get('/users', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/bigFileUpload', function(req, res, next) {
  

   var fstream;
        req.pipe(req.busboy);
        req.busboy.on('file', function (fieldname, file, filename) {
            console.log("Uploading: " + filename);

            //Path where image will be uploaded
            fstream = fs.createWriteStream(__dirname + '/' + filename);
            file.pipe(fstream);
            fstream.on('close', function () {    
                console.log("Upload Finished of " + filename);              
                res.redirect('back');           //where to go next
            });
        });
});

router.get('/save', function(req, res, next) {
    MongoClient.connect('mongodb://localhost:27017/test', function (err, client) {
        if (err) throw err

        var db = client.db('test')

        db.collection('myCollection').insert({name: req.query.name});

        db.collection('myCollection').find().toArray(function (err, result) {
            if (err) throw err

            res.send(result);
        })
    })

    
});

module.exports = router;
