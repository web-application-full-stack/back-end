## Use
1. npm install  
2. DEBUG=myapp:* npm start

## JavaScript Manual
1. https://expressjs.com/en/guide/database-integration.html#mongodb  
2. https://github.com/mongodb/node-mongodb-native

## DB
#### Download
https://www.mongodb.com/download-center/community

#### Install
https://treehouse.github.io/installation-guides/mac/mongo-mac.html

#### MongoDB Shell Op
https://docs.mongodb.com/manual/mongo/
